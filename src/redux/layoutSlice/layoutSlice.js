import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  modal: { modalState: false, modalName: "" },
  isOpen: false,
  isSearch: false,
  inputValue: {
    status: "",
    priority: "",
  },
};

const layoutSlice = createSlice({
  name: "layoutSlice",
  initialState,
  reducers: {
    showModal: (state, { payload }) => {
      state.modal = {
        modalState: true,
        modalName: payload,
      };
    },
    closeModal: (state) => {
      state.modal = {
        modalState: false,
        modalName: "",
      };
    },
    updateDetailModalStatus: (state, { payload }) => {
      state.isOpen = payload;
    },
    updateSearchStatus: (state, { payload }) => {
      state.isSearch = payload;
    },
    updateStatusValue: (state, { payload }) => {
      state.inputValue.status = payload;
    },
    clearStatusValue: (state) => {
      state.inputValue.status = "";
    },
    updatePrioValue: (state, { payload }) => {
      state.inputValue.priority = payload;
    },
    clearPrioValue: (state) => {
      state.inputValue.priority = "";
    },
  },
});

export const {
  showModal,
  closeModal,
  updateDetailModalStatus,
  updateSearchStatus,
  updateStatusValue,
  updatePrioValue,
  clearStatusValue,
  clearPrioValue,
} = layoutSlice.actions;

export default layoutSlice.reducer;
