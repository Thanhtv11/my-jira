import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  allProject: null,
  projectCategory: null,
  User: [],
  detailOfProject: {},
  detailOfTasks: {},
  arrSearchItem: [],
  taskParams: {
    status: "",
    priority: "",
    type: "",
  },
  arrComments: [],
};

const projectSlice = createSlice({
  name: "projectSlice",
  initialState,
  reducers: {
    setAllProject: (state, { payload }) => {
      state.allProject = payload;
    },
    setProjectCategory: (state, { payload }) => {
      state.projectCategory = payload;
    },
    setUser: (state, { payload }) => {
      state.User = payload;
    },
    setDetailOfProject: (state, { payload }) => {
      state.detailOfProject = payload;
    },
    setDetailOfTasks: (state, { payload }) => {
      state.detailOfTasks = payload;
    },
    removePrevDetail: (state) => {
      state.detailOfProject = {};
    },
    setSearchProject: (state, { payload }) => {
      state.arrSearchItem = payload;
    },
    updateTaskParams: (state, { payload }) => {
      state.taskParams.status = payload[0].content;
      state.taskParams.priority = payload[1].content;
      state.taskParams.type = payload[2].content;
    },
    updateArrComments: (state, { payload }) => {
      state.arrComments = payload;
    },
  },
});

export const {
  setAllProject,
  setProjectCategory,
  setUser,
  setDetailOfProject,
  setDetailOfTasks,
  removePrevDetail,
  setSearchProject,
  updateTaskParams,
  updateArrComments,
} = projectSlice.actions;

export default projectSlice.reducer;
