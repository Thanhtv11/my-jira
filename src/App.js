import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { routes } from "./routes/routes";

function App() {
  const route = () =>
    routes.map((item, index) => (
      <Route key={index} path={item.path} element={item.element} />
    ));

  return (
    <div>
      <BrowserRouter>
        <Routes>{route()}</Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
