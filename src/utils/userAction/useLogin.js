import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setUserLoginInfo } from "../../redux/userSlice/userSlice";
import { userLocalStorage } from "../../services/user/user.localStorage";
import { userService } from "../../services/user/user.service";
import { openNotification } from "../openNotification/openNotification";

export const useLogin = () => {
  const history = useNavigate();
  const dispatch = useDispatch();

  const login = (userInput) => {
    userService
      .postUserSignIn(userInput)
      .then((success) => {
        const userInfo = success.data.content;

        userLocalStorage.userInfo.set(userInfo);
        dispatch(setUserLoginInfo(userInfo));
        console.log("userInfo: ", userInfo);
        history("/");

        openNotification({
          type: "success",
          message: "Login success!",
          description: "Getting your projects.",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: "Login fail!",
          description: error.response.data.message,
        });
      });
  };

  return login;
};
