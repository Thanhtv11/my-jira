import { useDispatch } from "react-redux";
import { removeUserInfo } from "../../redux/userSlice/userSlice";
import { userLocalStorage } from "../../services/user/user.localStorage";

export const useLogout = () => {
  const dispatch = useDispatch();

  const logout = () => {
    userLocalStorage.userInfo.remove();
    dispatch(removeUserInfo());
  };

  return logout;
};
