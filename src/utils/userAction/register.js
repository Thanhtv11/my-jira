import { userService } from "../../services/user/user.service";
import { openNotification } from "../openNotification/openNotification";

export const register = (userInput, onSuccess) => {
  userService
    .postUserSignUp(userInput)
    .then(() => {
      openNotification({
        type: "success",
        message: "Register success!",
        description: "Login automatically with your new account.",
      });
      onSuccess();
    })
    .catch((error) => {
      openNotification({
        type: "error",
        message: "Register fail!",
        description: error.response.data.message,
      });
    });
};
