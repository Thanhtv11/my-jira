import { setAllProject } from "../../redux/projectSlice/projectSlice";
import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";

export const getAllProject = () => {
  return (dispatch) => {
    projectService
      .getProjectGetAllProject()

      .then((success) => {
        dispatch(setAllProject(success.data.content));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get project fail!",
          description: error.response.data.message,
        });
      });
  };
};
