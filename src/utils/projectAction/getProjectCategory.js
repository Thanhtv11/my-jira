import { setProjectCategory } from "../../redux/projectSlice/projectSlice";
import { projectService } from "../../services/project/project.service";
import { openNotification } from "../openNotification/openNotification";

export const getProjectCategory = () => {
  return (dispatch) => {
    projectService
      .getProjectCategory()
      .then((success) => {
        const projectCategory = success.data.content;
        dispatch(setProjectCategory(projectCategory));
      })

      .catch((error) => {
        openNotification({
          type: "error",
          message: "Get project category fail!",
          description: error.response.data.message,
        });
      });
  };
};
