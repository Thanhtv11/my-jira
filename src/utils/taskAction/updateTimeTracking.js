import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getDetailOfProject } from "../projectAction/getDetailProject";

export const updateTimeTracking = (id, data) => {
  return (dispatch) => {
    taskService
      .updateTimeTracking(data)
      .then((success) => {
        dispatch(getDetailOfProject(id));
        openNotification({
          type: "success",
          message: "Update Time Tracking successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: `${error.response.data.content}`,
        });
      });
  };
};
