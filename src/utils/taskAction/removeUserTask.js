import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getDetailOfProject } from "../projectAction/getDetailProject";

export const removeUserFromTask = (id, data) => {
  return (dispatch) => {
    taskService
      .removeUserTask(data)
      .then((success) => {
        dispatch(getDetailOfProject(id));
        openNotification({
          type: "success",
          message: "Remove User From Task successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: `${error.response.data.content}`,
        });
      });
  };
};
