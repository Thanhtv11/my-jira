import { setProjectCategory } from "../../redux/projectSlice/projectSlice";
import { projectService } from "../../services/project/project.service";
import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllComments } from "./getAllComments";

export const deleteComment = (taskId, id) => {
  return (dispatch) => {
    taskService
      .deleteCmt(id)
      .then((success) => {
        dispatch(getAllComments(taskId));
        setTimeout(() => {
          openNotification({
            type: "success",
            message: "Delete comment succesfully",
          });
        }, 300);
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: error.response.data.message,
          //   description: error.response.data.message,
        });
      });
  };
};
