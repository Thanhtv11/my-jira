import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getDetailOfProject } from "../projectAction/getDetailProject";
import { getAllComments } from "./getAllComments";

export const insertCmt = (taskId, data) => {
  return (dispatch) => {
    taskService
      .insertCmt(data)
      .then((success) => {
        dispatch(getAllComments(taskId));
        openNotification({
          type: "success",
          message: "Insert comments successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: `${error.response.data.content}`,
        });
      });
  };
};
