import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getDetailOfProject } from "../projectAction/getDetailProject";

export const assignToTask = (id, data) => {
  return (dispatch) => {
    taskService
      .assignUserTask(data)
      .then((success) => {
        dispatch(getDetailOfProject(id));
        openNotification({
          type: "success",
          message: "Asign User Task successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: `${error.response.data.content}`,
        });
      });
  };
};
