import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getDetailOfProject } from "../projectAction/getDetailProject";

export const createNewTask = (id, data) => {
  return (dispatch) => {
    taskService
      .createTask(data)
      .then((success) => {
        dispatch(getDetailOfProject(id));
        openNotification({
          type: "success",
          message: "Create task successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: `${error.response.data.content}`,
        });
      });
  };
};
