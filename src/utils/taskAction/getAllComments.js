import { updateArrComments } from "../../redux/projectSlice/projectSlice";
import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getDetailOfProject } from "../projectAction/getDetailProject";

export const getAllComments = (data) => {
  return (dispatch) => {
    taskService
      .getAllComments(data)
      .then((success) => {
        dispatch(updateArrComments(success.data.content));
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: `${error.response.data.content}`,
        });
      });
  };
};
