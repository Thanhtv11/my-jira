import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getDetailOfProject } from "../projectAction/getDetailProject";

export const updatePriority = (id, data) => {
  return (dispatch) => {
    taskService
      .updatePriorityTask(data)
      .then((success) => {
        dispatch(getDetailOfProject(id));
        openNotification({
          type: "success",
          message: "Update Priority successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: ` ${error.response.data.content}`,
        });
      });
  };
};
