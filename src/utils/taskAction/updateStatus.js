import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getDetailOfProject } from "../projectAction/getDetailProject";

export const updateStatus = (id, data) => {
  return (dispatch) => {
    taskService
      .updateStatusTask(data)
      .then((success) => {
        dispatch(getDetailOfProject(id));
        openNotification({
          type: "success",
          message: "Update task status successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: "Update task status fail!!",
          description: ` ${error.response.data.creator}`,
        });
      });
  };
};
