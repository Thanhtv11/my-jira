import { setProjectCategory } from "../../redux/projectSlice/projectSlice";
import { projectService } from "../../services/project/project.service";
import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getAllComments } from "./getAllComments";

export const updateComment = (taskId, id, content) => {
  return (dispatch) => {
    taskService
      .updateCmt(id, content)
      .then((success) => {
        dispatch(getAllComments(taskId));
        openNotification({
          type: "success",
          message: "Update comment succesfully",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: error.response.data.message,
          //   description: error.response.data.message,
        });
      });
  };
};
