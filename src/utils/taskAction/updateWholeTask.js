import { closeModal } from "../../redux/layoutSlice/layoutSlice";
import { taskService } from "../../services/tasks/task.service";
import { openNotification } from "../openNotification/openNotification";
import { getDetailOfProject } from "../projectAction/getDetailProject";

export const updateWholeTask = (id, data) => {
  return (dispatch) => {
    taskService
      .editTask(data)
      .then((success) => {
        dispatch(getDetailOfProject(id));
        dispatch(closeModal());
        openNotification({
          type: "success",
          message: "Edit task status successfully!",
        });
      })
      .catch((error) => {
        openNotification({
          type: "error",
          message: "Edit task status fail!!",
        });
      });
  };
};
