import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import CardTask from "../../Components/Utils/Card/CardTask";
import Spinner from "../../Components/Utils/Spinner/Spinner";
import {
  removePrevDetail,
  updateTaskParams,
} from "../../redux/projectSlice/projectSlice";
import { https } from "../../services/configURL/configURL";
import { openNotification } from "../../utils/openNotification/openNotification";
import { getDetailOfProject } from "../../utils/projectAction/getDetailProject";
import { getUser } from "../../utils/projectAction/getUser";

export default function TaskPage() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const { detailOfProject } = useSelector((state) => state.projectSlice);
  const { lstTask, projectName } = detailOfProject;
  // console.log(detailOfProject);

  const handleSubmit = () => {};
  const handleChangeInput = () => {};

  useEffect(() => {
    setTimeout(() => {
      dispatch(getDetailOfProject(id));
      dispatch(getUser());
    }, 200);
    return () => {
      dispatch(removePrevDetail());
    };
  }, [id]);

  useEffect(() => {
    // console.log("rerender");
    const asyncFetchingData = async () => {
      try {
        const res = await Promise.all([
          https.get(`/api/Status/getAll`),
          https.get(`/api/Priority/getAll`),
          https.get(`/api/TaskType/getAll`),
        ]);
        dispatch(updateTaskParams(res.map((r) => r.data)));
        // console.log(res.map((r) => r.data));
      } catch (error) {
        openNotification({
          type: "error",
          message: error.message,
        });
      }
    };
    asyncFetchingData();
  }, [id, detailOfProject]);

  return (
    <>
      <div className="relative mb-4">
        <form onSubmit={handleSubmit} className="flex items-center">
          <svg
            className="w-5 h-5 text-gray-500 dark:text-gray-400 absolute ml-2"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
              clipRule={`evenodd`}
            ></path>
          </svg>
          <input
            onChange={(e) => handleChangeInput(e)}
            type="text"
            id="table-search"
            className="bg-white text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-80 pl-10 p-2.5 dark:border-gray-600 "
            placeholder="Search for tasks"
          />
        </form>
      </div>
      <div className="w-full text-center mx-auto">
        <span className="text-xl uppercase font-bold">{projectName}</span>
      </div>
      <div
        className={`grid mt-8
      } grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4 md:gap-x-5 xl:gap-0 mx-auto w-full h-full overflow-x-hidden`}
      >
        {lstTask?.length > 0 ? (
          lstTask?.map((t) => (
            <div
              key={t.statusId}
              className={`bg-gray-400/30 uppercase rounded-xl ${
                t.lstTaskDeTail && t.lstTaskDeTail.length > 0
                  ? "h-auto"
                  : "h-80"
              }   w-64 text-center flex flex-col py-2`}
            >
              <p className="text-xl font-bold">{t.statusName}</p>
              <div className="flex flex-col space-y-4">
                {t.lstTaskDeTail && t.lstTaskDeTail.length > 0 ? (
                  t.lstTaskDeTail.map((d, i) => (
                    <CardTask key={i} id={id} detail={d} />
                  ))
                ) : (
                  <span className="text-xs font-medium">
                    No Tasks Available
                  </span>
                )}
              </div>
            </div>
          ))
        ) : (
          <Spinner />
        )}
      </div>
    </>
  );
}
