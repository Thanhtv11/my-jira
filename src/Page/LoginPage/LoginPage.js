import React from "react";
import { Button, Form, Input } from "antd";
import { NavLink } from "react-router-dom";
import { useLogin } from "../../utils/userAction/useLogin";

export default function LoginPage() {
  const login = useLogin();

  const onFinish = (value) => {
    const { email, passWord } = value;
    const userInput = { email, passWord };

    login(userInput);
  };

  return (
    <>
      <Form
        name="login"
        layout="vertical"
        requiredMark={false}
        onFinish={onFinish}
        autoComplete="off"
        className="w-4/5"
      >
        <Form.Item
          label="Email:"
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your email!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password:"
          name="passWord"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item>
          <Button className="w-full mt-5" type="primary" htmlType="submit">
            Login
          </Button>

          <p className="mt-5 text-center">
            Don't have an account?
            <NavLink to="/register"> Register here.</NavLink>
          </p>
        </Form.Item>
      </Form>
    </>
  );
}
