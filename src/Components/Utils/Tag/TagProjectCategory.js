import React, { useMemo } from "react";
import { Tag } from "antd";
import { CodeOutlined, AppstoreOutlined, MobileOutlined } from "@ant-design/icons";
import { translateEN } from "../../../utils/translate/translateEN";

export default function TagProjectCategory({ category }) {
  const { categoryId, categoryName, id, projectCategoryName } = category;

  const tag = useMemo(() => {
    switch (categoryId || id) {
      case 1:
        return {
          icon: <CodeOutlined />,
          color: "processing",
        };

      case 2:
        return {
          icon: <AppstoreOutlined />,
          color: "success",
        };

      case 3:
        return {
          icon: <MobileOutlined />,
          color: "warning",
        };

      default:
        return {
          icon: "",
          color: "",
        };
    }
  }, [categoryId, id]);

  const { icon, color } = tag;

  return (
    <>
      <Tag
        icon={icon}
        color={color}
        className="items-center"
        style={{ display: "flex", padding: 4 }}
      >
        {categoryName ? translateEN(categoryName) : translateEN(projectCategoryName)}
      </Tag>
    </>
  );
}
