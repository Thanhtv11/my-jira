import React, { useMemo } from "react";
import { Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  clearInput,
  clearPrioValue,
  clearStatusValue,
  closeModal,
} from "../../../redux/layoutSlice/layoutSlice";
import { editTask, formCreateProject, formCreateTask } from "./myModalName";
import FormCreateTask from "../Form/FormCreateTask";
import FormCreateProject from "../Form/FormCreateProject";
import FormEditTask from "../Form/FormEditTask";

export default function MyModal() {
  const { modal } = useSelector((state) => state.layoutSlice);
  const { modalName, modalState } = modal;
  const dispatch = useDispatch();

  const handleCancel = () => {
    dispatch(closeModal());
    dispatch(clearStatusValue());
    dispatch(clearPrioValue());
  };

  const modalContent = useMemo(() => {
    switch (modalName) {
      case formCreateTask:
        return {
          title: "Create Task",
          content: <FormCreateTask />,
        };
      case editTask:
        return {
          title: "Edit Task",
          content: <FormEditTask />,
        };
      case formCreateProject:
        return { title: "Create Project", content: <FormCreateProject /> };

      default:
        return { title: "Modal", content: <></> };
    }
  }, [modalName]);

  const { title, content } = modalContent;

  return (
    <>
      <Modal
        title={title}
        open={modalState}
        onCancel={handleCancel}
        footer={null}
        width={800}
      >
        {content}
      </Modal>
    </>
  );
}
