import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { setDetailOfTasks } from "../../../redux/projectSlice/projectSlice";
import { useShowModal } from "../../../utils/layoutAction/useShowModal";
import { getAllComments } from "../../../utils/taskAction/getAllComments";

import { editTask } from "../MyModal/myModalName";

export default function CardTask({ id, detail }) {
  const dispatch = useDispatch();
  const {
    assigness,
    taskName,
    taskId,
    priorityTask,
    taskTypeDetail,
    description,
  } = detail;
  const handleShowModal = useShowModal();
  const handleEditTask = (id) => {
    dispatch(setDetailOfTasks(detail));
    dispatch(getAllComments(taskId))
    handleShowModal(editTask);
  };

  return (
    <div
      onClick={() => handleEditTask(id)}
      className="rounded-xl cursor-pointer transition-all duration-300 group hover:bg-slate-400 w-[90%] mx-auto p-2 bg-white relative overflow-hidden"
    >
      <div className="flex items-center gap-2 border-b-2 mb-2 pt-1">
        <span className="text-xs font-medium">Type:{""}</span>
        <p className="text-sm font-semibold m-0 group-hover:text-white">
          {taskTypeDetail.taskType}
        </p>
      </div>
      <div className="w-full pt-1  ">
        <div className="flex justify-between border-b-2 pb-2" key={taskId}>
          <p className="text-gray-800 text-left text-clip text-sm leading-5 font-bold w-[65%] shrink-0 group-hover:text-white">
            {taskName}
          </p>
          <div className="flex flex-col space-y-2 grow ">
            <div className="grid grid-cols-3 gap-1 justify-center">
              {assigness.length > 0 ? (
                assigness.length > 4 ? (
                  assigness
                    ?.slice(0, 4)
                    .map((a, i) => (
                      <img
                        key={i}
                        className="w-6 h-6 rounded-full object-cover"
                        src={a.avatar}
                        alt=""
                      ></img>
                    ))
                ) : (
                  assigness.map((a, i) => (
                    <img
                      key={i}
                      className="w-6 h-6 rounded-full object-cover"
                      src={a.avatar}
                      alt=""
                    />
                  ))
                )
              ) : (
                <span className="font-bold">No assigness</span>
              )}
            </div>
            <p className="py-1 px-2 rounded-full text-xs font-bold text-white bg-[conic-gradient(at_right,_var(--tw-gradient-stops))] from-indigo-200 via-slate-600 to-indigo-200">
              {priorityTask.priority}
            </p>
          </div>
        </div>
        <div className="text-left h-16">
          <span className="text-xs">Description: </span>
          <span
            className={`pl-1 ${
              description ? "text-sm" : "text-xs"
            } mt-3 font-semibold text-clip group-hover:text-white`}
          >
            {description !== ""
              ? description.length >= 30
                ? description.slice(0, 30).replace(/<(.|\n)*?>/g, "")
                : description
              : "No description yet"}
          </span>
        </div>
      </div>
    </div>
  );
}
