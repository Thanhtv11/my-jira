import React, { useMemo, useState } from "react";
import { Tooltip } from "antd";
import { useDispatch, useSelector } from "react-redux";
// import { getDetailOfProject } from "../../../utils/projectAction/getDetailProject";
import { setDetailOfProject } from "../../../redux/projectSlice/projectSlice";
import { updateDetailModalStatus } from "../../../redux/layoutSlice/layoutSlice";
import FormLoadMembers from "../Form/FormLoadMembers";
import FormAddUser from "../Form/FormAddUser";
import { deleteProject } from "../../../utils/projectAction/deleteProject";
import { useNavigate } from "react-router-dom";

const MemberTag = (name) => (
  <div
    className="w-6 h-6 flex justify-center items-center border-2 rounded-full bg-white text-blue-400"
    style={{ marginLeft: -6 }}
  >
    <span>{name}</span>
  </div>
);

export default function CardProjectInfo({ projectInfo }) {
  const { id, members, description, projectName } = projectInfo;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // console.log(members);

  const [isToggleMember, setIsToggleMember] = useState(false);
  const [toggleAddMember, setToggleAddMember] = useState(false);
  const { allProject } = useSelector((state) => state.projectSlice);
  const handleEditProject = (id) => {
    const detailItem = allProject.find((p) => p.id === id);
    dispatch(setDetailOfProject(detailItem));
    dispatch(updateDetailModalStatus(true));
  };

  const handleDeleteProject = (id) => {
    dispatch(deleteProject(id));
  };
  const handleClickViewMembers = () => {
    setIsToggleMember((prev) => !prev);
  };
  const handleClickAddMembers = () => {
    setToggleAddMember((prev) => !prev);
  };

  return (
    <div className="w-full h-full flex flex-col gap-y-3 pl-2 pt-3">
      <div className="flex items-center justify-between gap-x-1">
        <span
          onClick={() => navigate(`/detail/${id}`)}
          className="text-xl uppercase font-bold transition duration-300 hover:text-red-900  cursor-pointer"
        >
          {projectName.length >= 13
            ? ` ${projectName.slice(0, 13)}...`
            : projectName}
        </span>
        <div className="flex space-x-2">
          <button
            onClick={() => handleEditProject(id)}
            className=" outline-none"
          >
            <img
              className="w-7 h-7 cursor-pointer transition-all duration-300 hover:-translate-y-2 hover:opacity-80"
              src="https://cdn-icons-png.flaticon.com/512/1160/1160515.png"
              alt=""
            ></img>
          </button>
          <button
            className="outline-none"
            onClick={() => handleDeleteProject(id)}
          >
            <img
              className="w-7 h-7 cursor-pointer transition-all duration-300 hover:-translate-y-2 hover:opacity-80"
              src="https://cdn-icons-png.flaticon.com/512/5028/5028066.png"
              alt=""
            ></img>
          </button>
        </div>
      </div>
      <div className="flex  pt-1 relative">
        <span> Members: </span>
        <div className="flex -space-x-2 hover:scale-110 cursor-pointer">
          {members?.length > 0 ? (
            members
              .slice(0, 4)
              .map((m, i) => (
                <img
                  title={members.name}
                  className="ml-1 w-6 h-6 object-cover rounded-full self-center"
                  key={i}
                  src={m.avatar}
                  alt=""
                ></img>
              ))
          ) : (
            <span className="text-sm font-semibold ml-1 self-center">Creator Only</span>
          )}
        </div>
        <div className="ml-2 flex justify-center items-center space-x-1">
          <button onClick={handleClickViewMembers} className="outline-none">
            <img
              className={`w-5 h-5 self-center`}
              src="https://cdn-icons-png.flaticon.com/512/463/463612.png"
              alt=""
            ></img>
          </button>
          {isToggleMember && (
            <FormLoadMembers
              id={id}
              members={members}
              setIsToggleMember={setIsToggleMember}
            />
          )}
          <button onClick={handleClickAddMembers} className="outline-none">
            <img
              className={`w-5 h-5 self-center`}
              src="https://cdn-icons-png.flaticon.com/512/3522/3522554.png"
              alt=""
            ></img>
          </button>
          {toggleAddMember && <FormAddUser members={members} id={id} />}
        </div>
      </div>
      <div className="w-full overflow-hidden">
        Description:{" "}
        {description ? (
          description.length >= 28 ? (
            <span className="text-clip">
              {description.slice(0, 28).replace(/<(.|\n)*?>/g, "")}
              ...
            </span>
          ) : (
            <span>{description.replace(/<(.|\n)*?>/g, "")}</span>
          )
        ) : (
          <span>Boss has not posted yet.</span>
        )}
      </div>
    </div>
  );
}
