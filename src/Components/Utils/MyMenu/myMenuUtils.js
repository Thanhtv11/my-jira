export const getItem = (label, key, icon, disabled) => ({
  label,
  key,
  icon,
  disabled,
});

export const buttonKey = {
  linkToHomePage: "linkToHomePage",
  openSearchTaskModal: "openSearchTaskModal",
  openCreateTaskModal: "openCreateTaskModal",
  logout: "logout",
};
