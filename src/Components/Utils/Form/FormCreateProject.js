import React, { useMemo, useRef } from "react";
import { Button, Form, Input, Select } from "antd";
import MyTinyEditor from "../MyTinyEditor/MyTinyEditor";
import { useDispatch, useSelector } from "react-redux";
import TagProjectCategory from "../Tag/TagProjectCategory";
import { createProject } from "../../../utils/projectAction/createProject";



const { Option } = Select;

export default function FormCreateProject() {
  const editorRef = useRef(null);
  const dispatch = useDispatch();
  const { projectCategory } = useSelector((state) => state.projectSlice);

  const options = useMemo(
    () =>
      projectCategory &&
      projectCategory.map((item) => {
        const { id } = item;
        return (
          <Option key={id} value={id}>
            <TagProjectCategory category={item} />
          </Option>
        );
      }),
    [projectCategory]
  );

  const onFinish = (value) => {
    let description = "";
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
      description = editorRef.current.getContent();
    }

    const { projectName, categoryId } = value;

    const projectInfo = {
      projectName,
      description,
      categoryId,
      alias: "",
    };

    console.log("projectInfo: ", projectInfo);
    dispatch(createProject(projectInfo));
  };

  return (
    <>
      <Form
        name="create project"
        layout="vertical"
        requiredMark={false}
        onFinish={onFinish}
        autoComplete="off"
        fields={[{ name: ["categoryId"], value: 1 }]}
      >
        <Form.Item
          name="projectName"
          rules={[
            {
              required: true,
              message: "Please input your project name!",
            },
          ]}
          className="custom-explain-error"
        >
          <Input bordered={false} placeholder="Project name" />
        </Form.Item>

        <Form.Item name="categoryId">
          <Select bordered={false} style={{ width: 180 }}>
            {options}
          </Select>
        </Form.Item>

        <MyTinyEditor editorRef={editorRef} placeholder="Project description" />

        <Form.Item className="flex justify-end">
          <Button type="primary" htmlType="submit" className="mt-5">
            Create project
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}
