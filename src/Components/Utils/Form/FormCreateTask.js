import React, { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import MyTinyEditor from "../MyTinyEditor/MyTinyEditor";
import { Form, Input, Button, Select } from "antd";
import { useShowModal } from "../../../utils/layoutAction/useShowModal";
import { useDispatch, useSelector } from "react-redux";
import { createNewTask } from "../../../utils/taskAction/createTask";
import { openNotification } from "../../../utils/openNotification/openNotification";

export default function FormCreateTask() {
  const dispatch = useDispatch();
  const { id } = useParams();

  const editorRef = useRef(null);
  const { Option } = Select;
  const handleShowModal = useShowModal();
  const [taskObj, setTaskObj] = useState({
    listUserAsign: [0],
    taskName: "",
    description: "",
    statusId: 0,
    originalEstimate: 0,
    timeTrackingSpent: 0,
    timeTrackingRemaining: 0,
    projectId: id,
    typeId: 0,
    priorityId: 0,
  });
  const { taskParams } = useSelector((state) => state.projectSlice);
  const { User } = useSelector((state) => state.projectSlice);
  const { status, priority, type } = taskParams;
  const onChangePrio = (value) => {
    setTaskObj({ ...taskObj, priorityId: value });
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };
  // onChange={(e) => handleChangeInput(e)}
  const onChangeStatus = (value) => {
    setTaskObj({ ...taskObj, statusId: value });
  };
  const onChangeTaskType = (value) => {
    setTaskObj({ ...taskObj, typeId: value });
  };
  const handleAsign = () => {};
  const onChangeAsignees = (value) => {
    // let arrUser = [];
    // value.map((v) => {
    //   return arrUser.push({
    //     userId: v.toString(),
    //   });
    // });
    // setTaskObj({
    //   ...taskObj,
    //   listUserAsign: arrUser,
    // });
  };

  useEffect(() => {}, []);

  const handleChangeInput = (e) => {
    setTaskObj({ ...taskObj, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let description = "";
    if (editorRef.current.getContent() && taskObj.taskName) {
      description = editorRef.current
        .getContent()
        .replace(/(&nbsp;)*/g, "")
        .replace(/(<p>)*/g, "")
        .replace(/<(\/)?p[^>]*>/g, "");

      dispatch(createNewTask(id, { ...taskObj, description }));
    } else {
      openNotification({
        type: "error",
        message: "Please fill out all inputs",
      });
    }

    // console.log(description);
  };
  return (
    <div className="rounded-lg bg-gray-300 px-7 py-2 shadow-lg lg:col-span-3 lg:px-12 lg:py-5">
      <form onSubmit={handleSubmit} className="space-y-4 pt-1">
        <div>
          <input
            onChange={(e) => handleChangeInput(e)}
            className="w-full outline-none border-gray-200 h-8 p-3 text-sm"
            placeholder="Task Name"
            type="text"
            name="taskName"
          />
        </div>
        <div className="grid grid-cols-1 gap-4 sm:grid-cols-2">
          <div className="self-center status">
            <Select
              className="block w-full"
              showSearch
              placeholder={`Status`}
              optionFilterProp="children"
              onChange={onChangeStatus}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().includes(input.toLowerCase())
              }
            >
              {status?.length > 0 &&
                status.map(({ statusId, statusName }) => (
                  <Option key={statusId} value={statusId}>
                    {statusName}
                  </Option>
                ))}
            </Select>
          </div>
          <div className="time-tracking grid grid-cols-2 gap-2">
            <input
              onChange={(e) => handleChangeInput(e)}
              className="w-full col-span-2 outline-none border-gray-200 h-8 p-3 text-sm"
              placeholder="Time tracking"
              type="text"
              name="originalEstimate"
            />
            <input
              onChange={(e) => handleChangeInput(e)}
              className="w-full outline-none border-gray-200 h-8 p-3 text-sm"
              placeholder="Time spent"
              type="text"
              name="timeTrackingSpent"
            />
            <input
              onChange={(e) => handleChangeInput(e)}
              className="w-full outline-none border-gray-200 h-8 p-3 text-sm"
              placeholder="Time remaining"
              type="text"
              name="timeTrackingRemaining"
            />
          </div>
        </div>
        <div className="grid grid-cols-1 gap-4 text-center sm:grid-cols-3">
          <div className="asignees">
            <Select
              className="w-full block"
              mode="multiple"
              placeholder="Asignees"
              optionFilterProp="children"
              onChange={onChangeAsignees}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().includes(input.toLowerCase())
              }
            >
              {User?.length > 0 &&
                User?.slice(0, 80).map((u) => (
                  <Option
                    onClick={() => handleAsign(u.userId)}
                    key={u.userId}
                    value={u.userId}
                  >
                    {u.name}
                  </Option>
                ))}
            </Select>
          </div>
          <div className="priority">
            <Select
              className="block w-full "
              showSearch
              placeholder="Priority"
              optionFilterProp="children"
              onChange={onChangePrio}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().includes(input.toLowerCase())
              }
            >
              {priority?.length > 0 &&
                priority.map(({ priorityId, priority }) => (
                  <Option key={priorityId} value={priorityId}>
                    {priority}
                  </Option>
                ))}
            </Select>
          </div>
          <div className="type">
            <Select
              className="block w-full"
              showSearch
              placeholder={`Task Type`}
              optionFilterProp="children"
              onChange={onChangeTaskType}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().includes(input.toLowerCase())
              }
            >
              {type?.length > 0 &&
                type.map((t) => (
                  <Option key={t.id} value={t.id}>
                    {t.taskType}
                  </Option>
                ))}
            </Select>
          </div>
        </div>
        <MyTinyEditor editorRef={editorRef} placeholder="Task description" />
        <div className="mt-4">
          <button
            // onClick={() => handleOk()}
            type="submit"
            className="inline-flex w-full items-center outline-none justify-center bg-black px-5 py-3 text-white sm:w-auto"
          >
            <span className="font-medium"> Create </span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="ml-3 h-5 w-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M14 5l7 7m0 0l-7 7m7-7H3"
              />
            </svg>
          </button>
        </div>
      </form>
    </div>
  );
}
