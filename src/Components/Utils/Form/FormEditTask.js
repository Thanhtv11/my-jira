import React, { useRef, useState } from "react";
import { Select } from "antd";
import { useShowModal } from "../../../utils/layoutAction/useShowModal";
import MyTinyEditor from "../MyTinyEditor/MyTinyEditor";
import { useDispatch, useSelector } from "react-redux";
import {
  closeModal,
  updateStatusValue,
  updatePrioValue,
} from "../../../redux/layoutSlice/layoutSlice";
import { userLocalStorage } from "../../../services/user/user.localStorage";
import { updateStatus } from "../../../utils/taskAction/updateStatus";
import { useParams } from "react-router-dom";
import { updatePriority } from "../../../utils/taskAction/updatePriority";
import { assignToTask } from "../../../utils/taskAction/assignUserTask";
import { removeUserFromTask } from "../../../utils/taskAction/removeUserTask";
import { openNotification } from "../../../utils/openNotification/openNotification";
import { updateTimeTracking } from "../../../utils/taskAction/updateTimeTracking";
import { updateDescription } from "../../../utils/taskAction/updateDescription";
import { insertCmt } from "../../../utils/taskAction/insertCmt";
import { deleteComment } from "../../../utils/taskAction/deleteCmt";
import { updateComment } from "../../../utils/taskAction/updateComment";
import { updateTimeEstimate } from "../../../utils/taskAction/updateTimeEstimate";

export default function FormEditTask() {
  const { id } = useParams();
  const user = userLocalStorage.userInfo.get();
  const { detailOfTasks } = useSelector((state) => state.projectSlice);
  const { User } = useSelector((state) => state.projectSlice);
  const { taskParams } = useSelector((state) => state.projectSlice);
  const { status, priority, type } = taskParams;
  const { inputValue } = useSelector((state) => state.layoutSlice);
  const [comment, setComment] = useState("");
  const [editCmt, setEditCmt] = useState("");
  const [isEdit, setIsEdit] = useState(false);
  const { arrComments } = useSelector((state) => state.projectSlice);
  const {
    assigness,
    taskName,
    taskId,
    statusId,
    priorityTask,
    taskTypeDetail,
    description,
    timeTrackingRemaining,
    originalEstimate,
    timeTrackingSpent,
  } = detailOfTasks;
  // console.log(detailOfTasks);
  const [taskObj, setTaskObj] = useState({
    listUserAsign: [0],
    taskId,
    taskName: "",
    description: "",
    statusId: "",
    originalEstimate: 0,
    timeTrackingSpent: 0,
    timeTrackingRemaining: 0,
    projectId: id,
    typeId: 0,
    priorityId: 0,
  });
  const handleClickComment = () => {
    if (comment) {
      dispatch(
        insertCmt(taskId, {
          taskId,
          contentComment: comment,
        })
      );
      setComment("");
    }
  };
  const handleChangeComment = (e) => {
    setComment(e.target.value);
  };
  const handleChangeEditComment = (e) => {
    setEditCmt(e.target.value);
  };
  const statusPlaceHolder = status.find((s) => s.statusId === statusId);
  const dispatch = useDispatch();
  const editorRef = useRef(null);
  const { Option } = Select;
  // const handleShowModal = useShowModal();
  const handleEditComment = (id) => {
    setIsEdit((prev) => !prev);
  };
  const handleDeleteComment = (id) => {
    dispatch(deleteComment(taskId, id));
  };
  const handleSubmitChangeCmt = (id) => {
    if (editCmt) {
      dispatch(updateComment(taskId, id, editCmt));
      setIsEdit(false);
      setEditCmt("");
    } else {
      setIsEdit(false);
    }
  };
  const handleClickCancle = () => {
    dispatch(closeModal());
  };
  const handleChangeTimeTracking = (e) => {
    setTaskObj({ ...taskObj, [e.target.name]: e.target.value });
  };
  const submitTimeTracking = () => {
    dispatch(
      updateTimeTracking(id, {
        taskId,
        timeTrackingSpent: taskObj.timeTrackingSpent,
        timeTrackingRemaining: taskObj.timeTrackingRemaining,
      })
    );
  };
  const handleChangeTimeEstimate = (e) => {
    dispatch(
      updateTimeEstimate(id, {
        taskId,
        originalEstimate: e.target.value || 0,
      })
    );
    // setTaskObj({});
  };
  const onChangeStatus = (value) => {
    setTaskObj({ ...taskObj, statusId: value });
    dispatch(updateStatus(id, { taskId, statusId: value }));
    dispatch(updateStatusValue(value));
  };
  const onChangeAsignees = (value) => {
    dispatch(assignToTask(id, { taskId, userId: value }));
  };

  const handleRemove = (value) => {
    dispatch(removeUserFromTask(id, { taskId, userId: value }));
  };
  // const onChangeTaskType = (value) => {
  //   setTaskObj({ ...taskObj, typeId: value });
  // };
  const onChangePriority = (value) => {
    setTaskObj({ ...taskObj, priorityId: value });
    dispatch(updatePriority(id, { taskId, priorityId: value }));
    dispatch(updatePrioValue(value));
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };
  const onFinish = (value) => {
    let description = "";
    if (editorRef.current.getContent()) {
      description = editorRef.current
        .getContent()
        .replace(/(&nbsp;)*/g, "")
        .replace(/(<p>)*/g, "")
        .replace(/<(\/)?p[^>]*>/g, "");
      dispatch(
        updateDescription(id, {
          taskId,
          description,
        })
      );
    } else {
      openNotification({
        type: "error",
        message: "Please fill out description",
      });
    }
  };

  return (
    <div className="w-full overflow-hidden">
      {/* <Select
        className="block w-28"
        showSearch
        placeholder={taskTypeDetail.taskType}
        optionFilterProp="children"
        onChange={onChangeTaskType}
        onSearch={onSearch}
        filterOption={(input, option) =>
          option.children.toLowerCase().includes(input.toLowerCase())
        }
      >
        {type?.length > 0 &&
          type.map((t) => (
            <Option key={t.id} value={t.id}>
              {t.taskType}
            </Option>
          ))}
      </Select> */}
      <span className="text-base font-semibold">{taskTypeDetail.taskType}</span>
      <div className="flex space-x-7 mt-5">
        <div className="w-[65%]">
          <p className="text-lg font-bold">{taskName}</p>
          <div>
            <p>Description</p>
            <MyTinyEditor editorRef={editorRef} placeholder={description} />
            <button
              onClick={onFinish}
              type="button"
              className="btn bg-blue-500 hover:bg-blue-600 mt-4 text-white px-4 py-2 font-medium rounded"
            >
              Save
            </button>
          </div>
          <div className="mt-4">
            <p>Comments</p>
            <div className="w-full flex flex-col space-y-4">
              <div className="flex items-center space-x-3 w-full">
                <img
                  className="w-8 h-8 rounded-full object-cover self-start"
                  src={user.avatar}
                  alt=""
                ></img>
                <div className="w-full">
                  <p>{user.name}</p>
                  <div>
                    <input
                      value={comment}
                      onChange={handleChangeComment}
                      placeholder="Comment something"
                      type="text"
                      className="w-full h-12 bg-gray-100 rounded p-2 mr-4 border focus:outline-none focus:border-blue-500"
                      name="contentComment"
                    />
                    <div className="flex justify-start mt-4 space-x-2">
                      <button
                        onClick={handleClickComment}
                        type="button"
                        className="btn bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 font-medium rounded"
                      >
                        Save
                      </button>
                      <button
                        onClick={handleClickCancle}
                        type="button"
                        className="btn bg-gray-200 hover:bg-gray-300 px-4 py-2 font-medium rounded"
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="comment flex flex-col space-y-5">
                {arrComments?.length > 0 &&
                  arrComments.map((c) => (
                    <div
                      key={c.id}
                      className="flex items-center space-x-3 w-full"
                    >
                      <img
                        className="w-8 h-8 rounded-full object-cover self-start"
                        src={c.user.avatar}
                        alt=""
                      ></img>
                      <div className="w-full flex flex-col space-y-3">
                        <div className="flex">
                          <span className="font-semibold text-base">
                            {c.user.name}
                          </span>
                        </div>
                        {isEdit && c.userId === user.id ? (
                          <input
                            value={editCmt}
                            onChange={handleChangeEditComment}
                            placeholder={c.contentComment}
                            type="text"
                            className="w-full h-12 bg-gray-100 rounded p-2 mr-4 border focus:outline-none focus:border-blue-500"
                            name="contentComment"
                          />
                        ) : (
                          <span className="text-sm">{c.contentComment}</span>
                        )}
                        {c.userId === user.id && (
                          <div className="flex space-x-3 flex-row mt-3">
                            {!isEdit ? (
                              <button
                                onClick={() => handleEditComment(c.id)}
                                className="w-16 bg-blue-500 text-white rounded-md cursor-pointer bg-opacity-90 hover:bg-opacity-100 transition-opacity duration-300 py-2"
                              >
                                Edit
                              </button>
                            ) : (
                              <button
                                onClick={() => handleSubmitChangeCmt(c.id)}
                                className="w-16 bg-blue-500 text-white rounded-md cursor-pointer bg-opacity-90 hover:bg-opacity-100 transition-opacity duration-300 py-2"
                              >
                                Save
                              </button>
                            )}
                            <button
                              onClick={() => handleDeleteComment(c.id)}
                              className="w-16 bg-gray-200 text-black rounded-md cursor-pointer bg-opacity-90 hover:bg-opacity-100 transition-opacity duration-300 py-2"
                            >
                              Delete
                            </button>
                          </div>
                        )}
                      </div>
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col space-y-6 max-w-[35%] grow">
          <div className="flex flex-col space-y-3 ">
            <span className="text-base uppercase font-semibold">Status</span>
            <Select
              value={inputValue.status}
              className="block w-full"
              showSearch
              placeholder={statusPlaceHolder.statusName}
              optionFilterProp="children"
              onChange={onChangeStatus}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().includes(input.toLowerCase())
              }
            >
              {status?.length > 0 &&
                status.map(({ statusId, statusName }) => (
                  <Option key={statusId} value={statusId}>
                    {statusName}
                  </Option>
                ))}
            </Select>
          </div>
          <div className="flex flex-col space-y-2">
            <span className="text-base uppercase font-semibold">Asignees</span>
            <div className="grid grid-cols-2 gap-2 max-w-xs">
              {assigness?.length > 0 &&
                assigness.map((a) => (
                  <div
                    key={a.id}
                    className="flex items-center flex-row space-x-1 w-full bg-gray-400 shadow-sm rounded p-1"
                  >
                    <img
                      className="w-6 h-6 rounded-full"
                      src={a.avatar}
                      alt=""
                    ></img>
                    <span className="text-sm text-white">{a.name}</span>
                    <img
                      onClick={() => handleRemove(a.id)}
                      className="w-4 h-4 rounded-full cursor-pointer"
                      src={`https://cdn-icons-png.flaticon.com/512/8371/8371096.png`}
                      alt=""
                    ></img>
                  </div>
                ))}
            </div>
            <Select
              style={{ width: 262 }}
              mode="single"
              showSearch
              placeholder=""
              optionFilterProp="children"
              onChange={onChangeAsignees}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().includes(input.toLowerCase())
              }
            >
              {User?.length > 0 &&
                User?.slice(0, 80).map((u) => (
                  <Option key={u.userId} value={u.userId}>
                    {u.name}
                  </Option>
                ))}
            </Select>
          </div>
          <div className="flex flex-col space-y-2">
            <span className="text-base uppercase font-semibold">Priority</span>
            <Select
              value={inputValue.priority}
              className="block w-full"
              showSearch
              placeholder={priorityTask.priority}
              optionFilterProp="children"
              onChange={onChangePriority}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().includes(input.toLowerCase())
              }
            >
              {priority?.length > 0 &&
                priority.map(({ priorityId, priority }) => (
                  <Option key={priorityId} value={priorityId}>
                    {priority}
                  </Option>
                ))}
            </Select>
          </div>
          <div className="time-tracking flex flex-col space-y-4">
            <div>
              <label>
                Time tracking:{""} {originalEstimate}
              </label>
              <input
                onChange={(e) => handleChangeTimeEstimate(e)}
                className="w-full border outline-none mt-2 border-gray-300 rounded-sm h-8 p-3 text-sm"
                type="text"
                name="originalEstimate"
              />
            </div>
            <div className="border border-black p-3">
              <div className="grid grid-cols-2 gap-2">
                <div>
                  <label>
                    Time spent:{""} {timeTrackingSpent}
                  </label>
                  <input
                    onChange={(e) => handleChangeTimeTracking(e)}
                    className="w-full border outline-none mt-2 border-gray-300 rounded-sm h-8 p-3 text-sm"
                    type="text"
                    name="timeTrackingSpent"
                  />
                </div>
                <div>
                  <label>
                    Time remaining:{""} {timeTrackingRemaining}
                  </label>
                  <input
                    onChange={(e) => handleChangeTimeTracking(e)}
                    className="w-full border outline-none mt-2 border-gray-300 rounded-sm h-8 p-3 text-sm"
                    type="text"
                    name="timeTrackingRemaining"
                  />
                </div>
              </div>
              <button
                onClick={submitTimeTracking}
                className="w-full bg-black text-white rounded-md p-2 mt-4 mx-auto"
              >
                Done
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
