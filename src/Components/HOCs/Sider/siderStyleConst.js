export const inlineStyle = {
  layout01: { minHeight: "100vh" },
  sider: {
    height: "100vh",
    position: "fixed",
    left: 0,
    top: 0,
    bottom: 0,
    overflow: "auto",
    zIndex: 99,
  },
  layout02: {
    minWidth: "max-content",
    marginLeft: 80,
  },
  content: {
    margin: "0 16px",
  },
  breadcrumb: {
    margin: "16px 0",
  },
};
