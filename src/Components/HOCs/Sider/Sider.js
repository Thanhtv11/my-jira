import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import { openNotification } from "../../../utils/openNotification/openNotification";
import SiderLayout from "./SiderLayout";

export default function Sider({ Component }) {
  const { userInfo } = useSelector((state) => state.userSlice);

  useEffect(() => {
    if (!userInfo) {
      openNotification({
        type: "default",
        message: "Must login first!",
        description: "Navigating to login page.",
      });
    }
  }, [userInfo]);

  return (
    <>
      {userInfo ? (
        <>
          <SiderLayout Component={Component} />
        </>
      ) : (
        <Navigate to="/login" />
      )}
    </>
  );
}
