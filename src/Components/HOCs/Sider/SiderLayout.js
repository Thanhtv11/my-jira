import React, { useState } from "react";
import { Breadcrumb, Layout } from "antd";
import MyModal from "../../Utils/MyModal/MyModal";
import MyMenu from "../../Utils/MyMenu/MyMenu";
import { inlineStyle } from "./siderStyleConst";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

const { Content, Sider } = Layout;
const { layout01, sider, layout02, content, breadcrumb } = inlineStyle;

export default function SiderLayout({ Component }) {
  const [collapsed, setCollapsed] = useState(true);
  const { userInfo } = useSelector((state) => state.userSlice);
  const { projectID } = useParams();

  return (
    <>
      <Layout style={layout01} hasSider>
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
          style={sider}
        >
          <div className="h-full flex flex-col justify-between">
            <MyMenu items={"upperMenuItems"} />
            <MyMenu items={"belowMenuItems"} />
          </div>
        </Sider>
        <Layout className="site-layout" style={layout02}>
          <Content style={content}>
            <div className="container mx-auto">
              <Breadcrumb style={breadcrumb}>
                <Breadcrumb.Item>{userInfo.name}</Breadcrumb.Item>

                {projectID ? (
                  <Breadcrumb.Item>project {projectID}</Breadcrumb.Item>
                ) : (
                  <Breadcrumb.Item>Home Page</Breadcrumb.Item>
                )}
              </Breadcrumb>

              <div className="site-layout-background">{Component}</div>
            </div>
          </Content>
        </Layout>
      </Layout>

      <MyModal />
    </>
  );
}
