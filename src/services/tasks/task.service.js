import { https } from "../configURL/configURL";

export const taskService = {
  updateStatusTask: (data) => https.put("/api/Project/updateStatus", data),
  updatePriorityTask: (data) => https.put("/api/Project/updatePriority", data),
  editTask: (data) => https.put("/api/Project/updateTask", data),
  createTask: (data) => https.post("/api/Project/createTask", data),
  assignUserTask: (data) => https.post("/api/Project/assignUserTask", data),
  removeUserTask: (data) => https.post("/api/Project/removeUserFromTask", data),
  getAllComments: (taskId) => https.get(`/api/Comment/getAll?taskId=${taskId}`),
  insertCmt: (data) => https.post("/api/Comment/insertComment", data),
  deleteCmt: (id) => https.delete(`/api/Comment/deleteComment?idComment=${id}`),
  updateCmt: (idCmt,content) => https.put(`/api/Comment/updateComment?id=${idCmt}&contentComment=${content}`),
  updateTimeTracking: (data) =>
    https.put("/api/Project/updateTimeTracking", data),
  updateDescription: (data) =>
    https.put("/api/Project/updateDescription", data),
  updateEstimate: (data) => https.put("/api/Project/updateEstimate", data),
};
