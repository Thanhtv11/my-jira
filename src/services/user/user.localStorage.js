const userInfoKeyName = "jira.userInfo";

export const userLocalStorage = {
  userInfo: {
    set: (userInfo) => {
      const jsonData = JSON.stringify(userInfo);
      localStorage.setItem(userInfoKeyName, jsonData);
    },

    get: () => {
      const jsonData = localStorage.getItem(userInfoKeyName);
      return !jsonData ? null : JSON.parse(jsonData);
    },

    remove: () => {
      localStorage.removeItem(userInfoKeyName);
    },
  },
};
