import { https } from "../configURL/configURL";

export const userService = {
  postUserSignIn: (userInput) => https.post("/api/Users/signin", userInput),
  postUserSignUp: (userInput) => https.post("/api/Users/signup", userInput),
};
