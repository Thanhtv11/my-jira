import axios from "axios";
import { userLocalStorage } from "../user/user.localStorage";
// const token = userLocalStorage.userInfo.get().accessToken;

const tokenCybersoft =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzMSIsIkhldEhhblN0cmluZyI6IjE5LzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Njc2NDgwMDAwMCIsIm5iZiI6MTY0ODQwMDQwMCwiZXhwIjoxNjc2OTEyNDAwfQ.2Pn1sQiOcYDhAQ2DqfnG78MdznvbWOk0pOmrJLVW9hs";

export const https = axios.create({
  baseURL: "https://jiranew.cybersoft.edu.vn",
  headers: {
    "Content-Type": "application/json",
    TokenCybersoft: tokenCybersoft,
    // Authorization: `Bearer ${token ? token : ""}`,
  },

  transformRequest: [
    function (data, headers) {
      const user = userLocalStorage.userInfo.get();
      const getAccessToken = () => (user ? user.accessToken : "");
      headers["Authorization"] = `Bearer ${getAccessToken()}`;
      return JSON.stringify(data);
    },
  ],
});
